package edu.krkm.pz.list.test;

import edu.krkm.pz.list.core.List;
import edu.krkm.pz.list.interfaces.ListException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ListTest
{
    List<Integer> list = new List<>();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void add() throws ListException {
        //GIVEN
        thrown.expect(ListException.class);
        thrown.expectMessage("index exceeds array size");
        //WHEN
        list.add(6,6);
        //THAN
    }

    @Test
    public void addStart() throws ListException {
        //GIVEN
        int expectedSize = 1;
        int expectedValue = 20;
        //WHEN
        list.addStart(20);
        //THAN
        int receivedValue = list.getValue(0);
        int receivedSize = list.getCount();
        assertEquals(expectedSize, receivedSize);
        assertEquals(expectedValue,receivedValue);
    }

    @Test
    public void addEnd() throws ListException {
        //GIVEN
        int expectedSize = 1;
        int expectedValue = 100;
        //WHEN
        list.addEnd(100);
        //THAN
        int receivedValue = list.getValue(0);
        int receivedSize = list.getCount();
        assertEquals(expectedSize,receivedSize);
        assertEquals(expectedValue,receivedValue);
    }

    @Test
    public void clear() throws ListException
    {
        //GIVEN
        int expectedSize = 0;
        //WHEN
        list.clear();
        //THAN
        assertEquals(expectedSize,list.getCount());
    }

    @Test
    public void remove() throws ListException {
        //GIVEN
        thrown.expect(ListException.class);
        thrown.expectMessage("The array is empty.");
        //WHEN
        list.remove(9);
        //THAN
    }

    @Test
    public void update() throws ListException {
        //GIVEN
        list.addStart(20);
        list.addStart(21);
        int expectedValue = 200;
        //WHEN
        list.update(200,1);
        //THAN
        int receivedValue = list.getValue(1);
        assertEquals(expectedValue,receivedValue);
    }

    @Test
    public void getValue() throws ListException {
        //GIVEN
        list.addStart(20);
        list.add(200,1);
        int expectedValue = 200;
        //WHEN
        int receivedValue = list.getValue(1);
        //THAW
        assertEquals(expectedValue,receivedValue);
    }

    @Test
    public void getCount() throws ListException {
        //GIVEN
        list.addStart(45);
        list.addStart(38);
        //WHEN
        boolean result = list.getCount() > 0;
        //THAN
        assertTrue(result);
    }
}
