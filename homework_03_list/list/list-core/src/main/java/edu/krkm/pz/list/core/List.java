package edu.krkm.pz.list.core;

import edu.krkm.pz.list.interfaces.ListException;
import edu.krkm.pz.list.interfaces.ListInterface;

public class List<T> implements ListInterface<T>
{
    private Object[] arrayList = new Object[0];

    @Override
<<<<<<< HEAD
    public void add(T value, int index) throws ListException
    {
        Validator.isMore(index,arrayList.length,"index exceeds array size","Add","List");
        Validator.isLess(index,0,"negative meaning","Add","List");
=======
    public void add(T value, int index) throws ListException {
        Validator.more(index,arrayList.length,"index exceeds array size","Add","List");
        Validator.less(index,0,"negative meaning","Add","List");
>>>>>>> cab51731ced7f4050bcaeb2c9e0a2b62286cb250
        Object[] array = new Object[arrayList.length + 1];
        System.arraycopy(arrayList,0,array,0,index);
        array[index] = value;
        System.arraycopy(arrayList,index,array,index + 1,arrayList.length - index);
        arrayList = array;
    }

    @Override
<<<<<<< HEAD
    public void addStart(T value) throws ListException
    {
=======
    public void addStart(T value) throws ListException {
>>>>>>> cab51731ced7f4050bcaeb2c9e0a2b62286cb250
        add(value,0);
    }

    @Override
<<<<<<< HEAD
    public void addEnd(T value) throws ListException
    {
=======
    public void addEnd(T value) throws ListException {
>>>>>>> cab51731ced7f4050bcaeb2c9e0a2b62286cb250
        add(value, getCount());
    }

    @Override
<<<<<<< HEAD
    public void clear()
    {
=======
    public void clear() {
>>>>>>> cab51731ced7f4050bcaeb2c9e0a2b62286cb250
        arrayList = new Object[0];
    }

    @Override
<<<<<<< HEAD
    public T remove(int index) throws ListException
    {

        Validator.isEqual(arrayList.length, 0, "The array is empty.", "Remove", "List");
        Validator.isMore(index, arrayList.length, "index exceeds array size", "Remove", "List");
        Validator.isLess(index, 0, "negative meaning", "Remove", "List");
=======
    public T remove(int index) throws ListException {

        Validator.areEqual(arrayList.length, 0, "The array is empty.", "Remove", "List");
        Validator.more(index, arrayList.length, "index exceeds array size", "Remove", "List");
        Validator.less(index, 0, "negative meaning", "Remove", "List");
>>>>>>> cab51731ced7f4050bcaeb2c9e0a2b62286cb250

        T value = (T) arrayList[index];

        Object[] copy = new Object[arrayList.length - 1];
        System.arraycopy(arrayList, 0, copy, 0, index);
        System.arraycopy(arrayList, index + 1, copy, index, arrayList.length - index - 1);
        arrayList = copy;
        return value;
    }

    @Override
<<<<<<< HEAD
    public void update(T newValue, int index) throws ListException
    {
        Validator.isEqual(arrayList.length, 0, "The array is empty.", "Update", "List");
        Validator.isMore(index, arrayList.length, "index exceeds array size", "Update", "List");
        Validator.isLess(index, 0, "negative meaning", "Update", "List");
=======
    public void update(T newValue, int index) throws ListException {
        Validator.areEqual(arrayList.length, 0, "The array is empty.", "Update", "List");
        Validator.more(index, arrayList.length, "index exceeds array size", "Update", "List");
        Validator.less(index, 0, "negative meaning", "Update", "List");
>>>>>>> cab51731ced7f4050bcaeb2c9e0a2b62286cb250

        arrayList[index] = newValue;
    }

    @Override
<<<<<<< HEAD
    public T getValue(int index) throws ListException
    {

        Validator.isEqual(arrayList.length, 0, "The array is empty.", "GetValue", "List");
        Validator.isMore(index, arrayList.length, "index exceeds array size", "GetValue", "List");
        Validator.isLess(index, 0, "negative meaning", "Remove", "List");
=======
    public T getValue(int index) throws ListException {

        Validator.areEqual(arrayList.length, 0, "The array is empty.", "GetValue", "List");
        Validator.more(index, arrayList.length, "index exceeds array size", "GetValue", "List");
        Validator.less(index, 0, "negative meaning", "Remove", "List");
>>>>>>> cab51731ced7f4050bcaeb2c9e0a2b62286cb250
        return (T) arrayList[index];
    }

    @Override
<<<<<<< HEAD
    public int getCount()
    {
=======
    public int getCount() {
>>>>>>> cab51731ced7f4050bcaeb2c9e0a2b62286cb250
        return arrayList.length;
    }

    @Override
<<<<<<< HEAD
    public T[] getArray()
    {
=======
    public T[] getArray() {
>>>>>>> cab51731ced7f4050bcaeb2c9e0a2b62286cb250
        return (T[]) arrayList;
    }
}
