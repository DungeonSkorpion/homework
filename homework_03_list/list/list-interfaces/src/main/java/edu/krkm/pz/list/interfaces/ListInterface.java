package edu.krkm.pz.list.interfaces;

public interface ListInterface<T>
{
    void add(T value, int index) throws ListException;
    void addStart(T value) throws ListException;
    void addEnd(T value) throws ListException;
    void clear();
    T remove(int index) throws ListException;
    void update(T newValue, int index) throws ListException;
    T getValue(int index) throws ListException;
    int getCount();
    T[] getArray();
}
