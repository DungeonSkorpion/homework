package edu.krkm.pz.list.interfaces;

public class ListException extends Exception
{
    private String nameMethod;
    private String nameClass;

    public ListException(){}
    public ListException(String message)
    {
        super(message);
    }
    public ListException(String message,String nameMethod)
    {
        super(message);
        this.nameMethod = nameMethod;
    }
    public ListException(String message,String nameMethod,String nameClass)
    {
        super(message);
        this.nameMethod = nameMethod;
        this.nameClass = nameClass;
    }

    public String getNameMethod(){return this.nameMethod;}
    public String getNameClass(){return this.nameClass;}

    public String toString(){return "Message:" + getMessage() + " Method:" + getNameMethod() + " Class:" + getNameClass();}
}
